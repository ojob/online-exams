import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { InterceptorService } from './interceptor.service';
import { ExamsComponent } from './exams/exams.component';
import { NewExamComponent } from './new-exam/new-exam.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from './auth.guard';


const routes: Routes = [
  {path: '', redirectTo: '/exams', pathMatch: 'full'},
  {path: 'exams', component: ExamsComponent},
  {path: 'new-exam', component: NewExamComponent, canActivate: [AuthGuard]},
  {path: 'me', component: ProfileComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true,
    }
  ],
})
export class AppRoutingModule { }
