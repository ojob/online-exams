import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { Exam } from './exam.model';
import { ExamsApiService } from './exams-api.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-exams',
  templateUrl: './exams.component.html',
  styleUrls: ['./exams.component.sass']
})
export class ExamsComponent implements OnDestroy, OnInit {
  examsListSubs: Subscription;
  examsList: Exam[];

  constructor(
    private examsApi: ExamsApiService,
    public auth: AuthService,
  ) { }

  ngOnInit(): void {
    this.load();
  }

  ngOnDestroy(): void {
    this.examsListSubs.unsubscribe();
  }

  load() {
    this.examsListSubs = this.examsApi.getExams().subscribe(
      res => { this.examsList = res; },
      console.error);
  }

  markActive(examId: number, active: Boolean): void {
    this.examsApi.updateExam(examId, active).subscribe(
      () => { this.load() },
      console.error);
  }

  delete(examId: number): void {
    this.examsApi.deleteExam(examId).subscribe(
      () => { this.load() },
      console.error);
  }
}
