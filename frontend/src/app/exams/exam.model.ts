export class Exam {
  constructor(
    // using python variables naming
    public title: string,
    public description: string,
    public long_description?: string,
    public id?: number,
    public updated_at?: Date,
    public created_at?: Date,
    public last_updated_by?: string,
    public active?: boolean,
  ) {}
}
