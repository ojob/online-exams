import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { API_URL } from '../env';
import { Exam } from './exam.model';

@Injectable()
export class ExamsApiService {

  constructor(private http: HttpClient) {}

  private static _handleError(err: HttpErrorResponse ) {
    return throwError(err.message || 'Error: unable to complete request.');
  }

  // GET list of exams
  getExams(): Observable<Exam[]> {
    return this.http.get<Exam[]>(`${API_URL}/exams`)
      .pipe(
        catchError(ExamsApiService._handleError));
  }

  // POST create exam
  storeExam(exam: Exam): Observable<any> {
    return this.http.post(`${API_URL}/exams`, exam);
  }

  // POST change exam status
  updateExam(examId: number, active: Boolean): Observable<any> {
    return this.http.post(`${API_URL}/exams/${examId}`, {"active": active});
  }

  // DELETE delete exam
  deleteExam(examId: number): Observable<any> {
    return this.http.delete(`${API_URL}/exams/${examId}`)
  }
}
