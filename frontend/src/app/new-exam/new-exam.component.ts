import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Exam } from '../exams/exam.model';
import { ExamsApiService } from '../exams/exams-api.service';

@Component({
  selector: 'app-new-exam',
  templateUrl: './new-exam.component.html',
  styleUrls: ['./new-exam.component.sass']
})
export class NewExamComponent {
  exam: Exam = {
    title: '',
    description: '',
    long_description: '',
  };

  constructor(
    private examsApi: ExamsApiService,
    private router: Router,
  ) { }

  storeExam() {
    // TODO: get all inputs values, as keyup does not capture all editing events
    this.examsApi.storeExam(this.exam)
      .subscribe(
        () => this.router.navigate(['/']),
        error => alert(error.message)
      );
  }
}
