import json
import typing
from functools import wraps
from urllib.request import urlopen

from flask import request, _request_ctx_stack
from jose import jwt

AUTH0_DOMAIN = 'dev-58v3631x.eu.auth0.com'  # no leading https://
AUTH0_CRYPTO_ALGOS = ['RS256']
AUTH0_AUDIENCE = 'https://online-exams.bourgault.fr'  # full URL
AUTH0_ROLES_NAMESPACE = 'https://online-exams.com/roles'


class AuthError(Exception):
    def __init__(self, error, status_code):
        super().__init__()
        self.error = error
        self.status_code = status_code


def get_token_auth_header() -> str:
    """Obtains the access token from Authorization header."""
    auth: str = request.headers.get('Authorization', None)
    if not auth:
        raise AuthError(
            error=dict(
                code='authorization_header_missing',
                description="Authorization header is expected."),
            status_code=401)
    parts: typing.List[str] = auth.split()
    if parts[0].lower() != 'bearer':
        raise AuthError(
            error=dict(
                code='invalid_header',
                description="Authorization header must start with 'bearer'"),
            status_code=401)
    elif len(parts) == 1:
        raise AuthError(
            error=dict(code='invalid_header',
                       description="Token not found."),
            status_code=401)
    elif len(parts) > 2:
        raise AuthError(
            error=dict(
                code='invalid_header',
                description="Authorization header must be 'bearer <token>'."),
            status_code=401)
    token = parts[1]
    return token


def requires_auth(func):
    """Decorate *func* while etermining if the Access Token is valid."""

    @wraps(func)
    def wrapped_func(*args, **kwargs):
        # retrieve token from current request
        token: str = get_token_auth_header()
        # retrieve the assotiated JWT
        jsonurl: str = urlopen(f'https://{AUTH0_DOMAIN}/.well-known/jwks.json')
        jwks: dict = json.loads(jsonurl.read())
        unverified_header: dict = jwt.get_unverified_header(token)
        rsa_key = {}
        for key in jwks['keys']:
            if key['kid'] == unverified_header['kid']:
                rsa_key = {k: key[k]
                           for k in ('kty', 'kid', 'use', 'n', 'e')}
                break
        if rsa_key:
            try:
                payload = jwt.decode(
                    token,
                    rsa_key,
                    algorithms=AUTH0_CRYPTO_ALGOS,
                    audience=AUTH0_AUDIENCE,
                    issuer=f'https://{AUTH0_DOMAIN}/',
                )
            except jwt.ExpiredSignatureError:
                raise AuthError(
                    error=dict(
                        code='token_expired',
                        description="Token expired.",
                    ),
                    status_code=401)
            except jwt.JWTClaimsError:
                raise AuthError(
                    error=dict(
                        code='invalid_claims',
                        description=("Incorrect claims; please check audience "
                                     "and issuer."),
                    ),
                    status_code=401)
            except Exception as exc:
                raise AuthError(
                    error=dict(
                        code='invalid_header',
                        description=(
                            f"Unable to parse authentication token: {exc}"),
                    ),
                    status_code=400)  # Bad request
            _request_ctx_stack.top.current_user = payload
            return func(*args, **kwargs)

        # otherwise
        raise AuthError(
            error=dict(
                code='invalid_header',
                description="Unable to find the apprpriate key."),
            status_code=400)  # Bad Request

    return wrapped_func


def requires_role(required_role):
    """Allow function to be called only for users with *required_role*."""
    def wrapper(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            token = get_token_auth_header()
            unverified_claims = jwt.get_unverified_claims(token)
            # search current token fro the expected role
            if unverified_claims.get(AUTH0_ROLES_NAMESPACE):
                roles = unverified_claims[AUTH0_ROLES_NAMESPACE]
                if any(role == required_role for role in roles):
                    return func(*args, **kwargs)
            # if none found
            raise AuthError(
                error=dict(code='insufficient_roles',
                           description="User missing required role"),
                status_code=401)
        return wrapped
    return wrapper
