from sqlalchemy import Boolean, Column, String
from .entity import Entity, Base

from marshmallow import Schema, fields


class Exam(Entity, Base):
    """Database entity for handling Exams."""

    __tablename__ = 'exams'
    title = Column(String)
    description = Column(String)
    long_description = Column(String)
    active = Column(Boolean)

    def __init__(self, title, description, long_description, created_by):
        Entity.__init__(self, created_by)
        self.title = title
        self.description = description
        self.long_description = long_description
        self.active = False


class ExamSchema(Schema):
    """For serialization of :py:class:`Exam` to/from JSON."""

    id = fields.Number()
    title = fields.Str()
    description = fields.Str()
    long_description = fields.Str()
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
    last_updated_by = fields.Str()
    active = fields.Boolean()
