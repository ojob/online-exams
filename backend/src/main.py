from flask import Flask, jsonify, request
from flask_cors import CORS

from .entities.entity import Base, Session, engine
from .entities.exam import Exam, ExamSchema
from .auth import AuthError, requires_auth, requires_role

# creating Flask app
app = Flask(__name__)
CORS(app)  # enable Cross-Origin Resource Sharing

# generate database schema
Base.metadata.create_all(engine)


@app.route('/exams')
def get_exams():
    # start session and fetch exams
    session = Session()
    exams_objects = session.query(Exam).order_by(Exam.id).all()
    # transforming into JSON-serializable objects
    exams = ExamSchema(many=True).dump(exams_objects)
    session.close()
    # serialize as JSON
    return jsonify(exams)


@app.route('/exams', methods=['POST'])
@requires_auth
def store_exams():
    # mount exam object from POST request
    posted_exam = \
        ExamSchema(only=('title', 'description', 'long_description')) \
        .load(request.get_json())
    exam = Exam(**posted_exam, created_by="HTTP POST")
    # persist exam
    session = Session()
    session.add(exam)
    session.commit()
    # return created exam
    new_exam = ExamSchema().dump(exam)
    session.close()  # close only after
    return jsonify(new_exam), 201


@app.route('/exams/<exam_id>', methods=['POST'])
@requires_role('admin')
def update(exam_id):
    is_active = request.get_json()['active']
    # persist change
    session = Session()
    exam = session.query(Exam).filter_by(id=exam_id).first()
    exam.active = is_active
    session.commit()
    session.close()
    return jsonify(dict(updated="OK")), 200


@app.route('/exams/<exam_id>', methods=['DELETE'])
@requires_role('admin')
def delete_exam(exam_id):
    session = Session()
    exam = session.query(Exam).filter_by(id=exam_id).first()
    session.delete(exam)
    session.commit()
    session.close()
    return '', 201


@app.errorhandler(AuthError)
def handle_auth_error(exc):
    response = jsonify(exc.error)
    response.status_code = exc.status_code
    return response
