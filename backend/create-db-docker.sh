#!/bin/bash
# creating the docker image for Postgres
docker run \
  --name online-exam-db \
  -p 5432:5432 \
  -e POSTGRES_DB=online-exam \
  -e POSTGRES_PASSWORD=0NLINE3-exa4m \
  -d \
  postgres

# to launch it:
# docker start online-exam-db
