"""renaming 'deleted' field to 'active'

Revision ID: 3528d5a1f067
Revises: ba7d9cb43751
Create Date: 2020-05-18 20:02:12.094330

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '3528d5a1f067'
down_revision = 'ba7d9cb43751'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column(
        'exams',
        column_name='deleted',
        new_column_name='active',
    )


def downgrade():
    op.alter_column(
        'exams',
        column_name='active',
        new_column_name='deleted',
    )
