"""add long_description to exams

Revision ID: ba7d9cb43751
Revises: ee8953911ada
Create Date: 2020-05-10 15:58:01.701235

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ba7d9cb43751'
down_revision = 'ee8953911ada'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        'exams',
        sa.Column(
            'long_description',
            sa.Text,
            nullable=False,
            server_default="Default exam description",
        )
    )


def downgrade():
    op.drop_column('exams', 'long_description')
