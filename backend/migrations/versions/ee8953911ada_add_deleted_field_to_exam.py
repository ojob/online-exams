"""add deleted field to Exam

Revision ID: ee8953911ada
Revises:
Create Date: 2020-05-09 08:14:32.927366

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ee8953911ada'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        'exams',
        sa.Column(
            'deleted',
            sa.Boolean,
            nullable=False,
            server_default='false',
        )
    )


def downgrade():
    op.drop_column('exams', 'deleted')
