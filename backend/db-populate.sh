#!/bin/bash
# populate db with one Exam, but without authentication
curl \
  -X POST \
  -H 'Content-Type: application/json' \
  -d '{
  "title": "TypeScript Advanced Exam",
  "description": "Tricky questions about TypeScript."
}' \
  http://0.0.0.0:5000/exams


# now with authentication
curl \
  --request POST \
  --url https://dev-58v3631x.eu.auth0.com/oauth/token \
  --header 'content-type: application/json' \
  --data '{
    "client_id":"9IUGHrmDDDSMtRRn4ssGqdHGYRbq9zOH",
    "client_secret":"aApMcFJ0bg5SJIhno1kJ-X39TeZxVLt7wRqTcyZPjfABQK4CHHXLzx8c5MUNb4Hc",
    "audience":"https://online-exams.bourgault.fr",
    "grant_type":"client_credentials"
  }'

JWT="eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlFDWUZ5V0VUX1pwMmluMFpWMnl4NiJ9.eyJpc3MiOiJodHRwczovL2Rldi01OHYzNjMxeC5ldS5hdXRoMC5jb20vIiwic3ViIjoiOUlVR0hybURERFNNdFJSbjRzc0dxZEhHWVJicTl6T0hAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vb25saW5lLWV4YW1zLmJvdXJnYXVsdC5mciIsImlhdCI6MTU4ODMyMTI4OSwiZXhwIjoxNTg4NDA3Njg5LCJhenAiOiI5SVVHSHJtREREU010UlJuNHNzR3FkSEdZUmJxOXpPSCIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.JKYDrw887Xf7XhVYfPGyvoZLsoRyjAUKAB8ze36tSzYuX3awGpC-WdoE-IFaMrS6PL-q6Fi8r9JEZn-fl8e26sI_8O7j3yVjQY0-5dXd5JRdN8aA4edr1UKqHsWRVAnsFKno3NILYVD_71xHKL7D6TPygG7pPRAdtLTfgMJrF4h3HlOO7z-wLkDxxsoNkP3FWkoIjb5W0r1qJaoMl0RaAsUU1zXmkiXEIYYndHBU65rJrZIMX6Vyr4orgWogfYwSrxNKAJ-6wltVzNv5EhgFptSL5MqeQmesUXKipMPwyPBYk24lkcXeIq9XybgjwPs8ZfFZEahVXY8Dow2eYmY1kg"

curl \
  -X POST \
  -H 'Content-Type: application/json' \
  -H 'Authorization: Bearer '$JWT \
  -d '{
  "title": "Auth Advanced Exam",
  "description": "Checking your knowledge about authentication."
}' \
  http://0.0.0.0:5000/exams
