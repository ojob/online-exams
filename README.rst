ONLINE-EXAM FULL-STACK APP
++++++++++++++++++++++++++

This project, shown to the user as a single-page app, has been initiated after
the `tutorial from Auth0.com`_, as an experience in full-stack development.

It glues together:

- Angular for the front-end, using many of its provided objects: Components,
  Directives, Services, Guards, Interceptors, Pipes... This is written in
  Typescript, a superset of Javascript.


- Flask for the back-end. This is written in Python.

- PostgreSQL for data persistence, running in a dedicated Docker container,
  consumed by the back-end using SQLAlchemy ORM, and version-managed 
  using Alembic.

- Auth0.com service for Authentication, using Javascript Web Tokens (JWT)
  for Authorization part (both in front-end and back-end).

Considering Angular and Auth0 services evolved significantly since the
tutorial was written, many personal addition and code-style updates have
been added.

.. _tutorial from Auth0.com: https://auth0.com/blog/using-python-flask-and-angular-to-build-modern-web-apps-part-3/

